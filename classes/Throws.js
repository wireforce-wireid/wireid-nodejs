#!/usr/bin/env node

/**
 * All errors messages in WireID framework
 */
class Throws {
    /**
     * This value assumes read-only capability and cannot be overwritten
     * @type {symbol}
     */
    static READ_ONLY = Symbol("This value assumes read-only capability and cannot be overwritten")

    /**
     * The set type of value does not match the required type
     * @type {symbol}
     */
    static TYPE_MISMATCH = Symbol("The set type of value does not match the required type")

    /**
     * The passed string is not a path entry
     * @type {symbol}
     */
    static VALUE_IS_NOT_PATH = Symbol("The passed string is not a path entry")
}

module.exports = Throws

