
class HTTPMethods {
    static POST = 'post'
    static GET = 'get'
    static PUT = 'put'
    static DELETE = 'delete'
}

module.exports = HTTPMethods