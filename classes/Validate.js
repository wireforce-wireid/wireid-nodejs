const jwt = require('jsonwebtoken');

class Validate {
    getJSONDataFromJWT(data) {
        return jwt.decode(data)
    }
}

module.exports = Validate