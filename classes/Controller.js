#!/usr/bin/env node

// WireID Framework
const Context = require("../classes/Context")
const Throws = require("../classes/Throws");
const Resources = require("../classes/Resources");
const GET = require("../classes/HTTPMethods").GET;
const RouterSettingsScheme = require("../classes/RouterSettingsScheme");

class Controller {

    /**
     * HTTP method
     * @type {string}
     */
    method = GET

    /**
     * Native middleware to express
     * @type {boolean}
     */
    native_middleware = false

    /**
     * Path to route
     * @type {string}
     */
    path = '/'

    /**
     * Parrent path
     * @type {string}
     */
    parentPath = '/'

    /**
     * Resources
     * @type {Resources|null}
     */
    #_R = null

    /**
     *
     * @type {Context|null}
     */
    #context = null

    /**
     * Get resources
     * @returns {Resources}
     * @constructor
     */
    get R() {
        return this.#_R;
    }

    /**
     * Get context
     * @returns {Context}
     */
    get context() {
        return this.#context
    }

    set context(value) {
        throw {
            name: Throws.READ_ONLY
        }
    }

    /**
     *
     * @param context {Context}
     * @param path {string|null}
     */
    constructor(context, path = null) {
        this.#context = context
        this.#_R = context.R
    }

    /**
     *
     * @param Request {Request}
     * @param Response {Response}
     */
    invoke(Request, Response) {

    }

    /**
     * Create scheme of controller
     * @returns {RouterSettingsScheme}
     */
    scheme() {
        const routerSettings = new RouterSettingsScheme();

        // Set router settings
        routerSettings.method = this.method
        routerSettings.path = this.path

        return routerSettings;
    }

    /**
     * Middleware
     * @param Request
     * @param Response
     * @param next
     */
    middleware(Request, Response, next) {
        next()
    }

}

module.exports = Controller

/**
 * Parse string URL to object
 * @param path {string}
 * @returns {{path: string, parentPath: string}}
 */
module.exports.parsePath = (path) => {
    const pathList = path.toString().split("/")

    if (pathList.length === 1) {
        return {
            parentPath: pathList[0].toString(),
            path: '/'
        }
    } else if (pathList.length === 2) {
        return {
            parentPath: pathList[0].toString(),
            path: pathList[1].toString()
        }
    } else if (pathList.length === 3) {
        if (pathList[1].toString() === "") {
            return {
                parentPath: pathList[1].toString(),
                path: pathList[2].toString()
            }
        } else {
            return {
                parentPath: pathList[0].toString(),
                path: pathList[1] + '/' + pathList[2]
            }
        }
    } else {
        throw {
            name: Throws.VALUE_IS_NOT_PATH
        }
    }
}