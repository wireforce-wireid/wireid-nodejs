#!/usr/bin/env node

// WireID Framework
const storage = new (require("../classes/Storage"))();
const validate = new (require("../classes/Validate"))();

class Middleware {

    validateLoginForm(req, res, next) {

        // Validate login
        if (!req.body.login) {
            return res.send({
                message: "Login is not defined"
            }, 500)
        }

        // Validate password
        if (!req.body.password) {
            return res.send({
                message: "Password is not defined"
            }, 500)
        }

        next()

    }

    validateCreateApplicationForm(req, res, next) {

        // Validate app name
        if (!req.body.app_name) {
            return res.send({ message: "Application name is not defined" }, 500)
        }

        // Validate redirect url
        if (!req.body.redirect_url) {
            return res.send({ message: "Redirect url is not defined" }, 500)
        }

        next()

    }

    validateCreateUserForm(req, res, next) {

        if (!req.body.first_name) {
            return res.send({ message: "Field `first_name` is not defined" }, 500)
        }
        if (!req.body.last_name) {
            return res.send({ message: "Field `last_name` is not defined" }, 500)
        }

        next()
    }

    validateAuth(req, res, next) {

        // Validate authorization
        if (!req.headers.authorization) {
            return res.send({ message: "Authorization token is not passed ih headers" }, 500)
        }

        try {
            const jwt_payload = validate.getJSONDataFromJWT(req.headers.authorization)

            storage.fetchUserCredentials(
                jwt_payload.public_secret,
                jwt_payload.secret_sign,

                function (error, userData) {
                    if (error === null) {
                        req.userData = userData
                        next()
                    } else {
                        res.json({ message: "User not found" }, 500)
                    }
                }
            )
        } catch (e) {
            return res.send({ message: "Authorization token is not valid" }, 500)
        }

    }
}

module.exports = Middleware