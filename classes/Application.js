#!/usr/bin/env node

const applicationModel = require('../models/application').applicationModel;
const rc4js = require("rc4js");
const crypto = require("crypto-js");
const fnv = require('fnv-plus');
const jwt = require('jsonwebtoken');

// WireID Framework
const storage = new (require("../classes/Storage"))()
const grant = new (require("../classes/Grant"))()

/**
 * Class for work with Application in ecosystem WireID framework
 */
class Application {
    async create(
        uuid,
        name,
        app_id,
        grants,
        owner,
        redirect_url
    ) {
        const application = new applicationModel({
            uuid,
            app_id,
            owner,
            redirect_url,

            name: name.toString().trim(),
            grants: grants.toString().split(","),
            trusted: [],
            rejected: false
        })

        return application.save()
    }

    tokenizeAccess(sign, user_uuid, callback) {
        const jwtSign = jwt.verify(
            sign,
            process.env.SECRET_JWT
        )

        storage.fetchApplication(jwtSign.app_id, function (app) {
            if (!app) {
                callback(false)
            } else {
                storage.fetchUser(user_uuid, function (user) {
                    if (!user) {
                        callback(false)
                    } else {
                        let signature = {}

                        for (const needGrant of Object.freeze(app.grants)) {
                            const grantParsed = grant.parse(needGrant)

                            switch (grantParsed.namespace) {
                                case 'user':
                                    if (typeof user[grantParsed.attr] != "undefined") {
                                        signature[grantParsed.attr] = user[grantParsed.attr]
                                    }
                            }
                        }

                        callback(null, {
                            redirect_url: app.redirect_url,
                            token: jwt.sign({
                                uuid: crypto.SHA1(user.uuid + jwtSign.app_id).toString(),
                                export: signature
                            }, process.env.SECRET_JWT, {
                                expiresIn: "90d"
                            })
                        })

                    }
                })
            }
        })
    }

    signTransactionAccess(app_id, public_secret) {
        return jwt.sign({
            app_id,
            public_secret,
        }, process.env.SECRET_JWT, {
            expiresIn: "30000"
        })
    }

    signApplicationId(uuid, private_secret) {
        const ahash52 = fnv.hash(uuid + crypto.MD5(private_secret).toString())

        return ahash52.hex()
    }
}

module.exports = Application