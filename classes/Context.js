#!/usr/bin/env node

// WireID Framework
const error = require("../classes/Throws")

const Resources = require("../classes/Resources");

class Context {

    /**
     * Resources
     * @type {Resources}
     * @private
     */
    #_R = null

    /**
     * Express app
     * @type {null}
     * @private
     */
    #_express = null

    /**
     * Express app
     * @returns {null}
     */
    get express() {
        return this.#_express;
    }

    /**
     * Set express app [NOT ALLOWED]
     * @param value
     */
    set express(value) {
        throw {
            name: error.READ_ONLY
        }
    }

    /**
     * Get resources
     * @returns {Resources}
     * @constructor
     */
    get R() {
        return this.#_R;
    }

    /**
     * Set resources [NOT ALLOWED]
     * @param value
     * @constructor
     */
    set R(value) {
        throw {
            name: error.READ_ONLY
        }
    }

    /**
     *
     * @param resourcesClass {Resources}
     * @param expressApp
     */
    constructor(
        resourcesClass,
        expressApp
    ) {
        this.#_express = expressApp

        if (resourcesClass instanceof Resources) {
            this.#_R = resourcesClass
        } else {
            throw {
                name: error.TYPE_MISMATCH
            }
        }
    }
}

module.exports = Context
