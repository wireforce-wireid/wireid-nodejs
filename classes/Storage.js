#!/usr/bin/env node

const credentialsModel = require('../models/credentials').credentialsModel;
const applicationModel = require('../models/application').applicationModel;
const userModel = require('../models/user').userModel;
const auth = new (require("../classes/Auth"))();

class Storage {

    fetchUserCredentials(
        public_secret,
        secret_sign,

        callback
    ) {
        credentialsModel.findOne({
            public_secret
        })
            .lean()
            .exec()
            .then(credentials_payload => {
                if (credentials_payload) {
                    const sign_server_data = auth.signSecretKeys(credentials_payload.private_secret, public_secret)

                    if (sign_server_data === secret_sign) {
                        callback(null, credentials_payload)
                    }
                } else {
                    callback(false)
                }
            })

    }

    countUserCredentials(
        login,
        password,

        callback
    ) {
        const keychain = auth.generateKeychain(login, password);
        const public_secret = auth.generatePublicSecret(keychain, password);

        credentialsModel.countDocuments({
            public_secret
        })
            .lean()
            .exec()
            .then(credentials_payload => {
                callback(credentials_payload)
            })

    }

    countApplicationCredentials(
        name,
        redirect_url,

        callback
    ) {
        applicationModel.countDocuments({
            name,
            redirect_url
        })
            .lean()
            .exec()
            .then(application_credentials => {
                callback(application_credentials)
            })

    }

    fetchApplication(
        app_id,

        callback
    ) {
        applicationModel.findOne({
            app_id
        })
            .lean()
            .exec()
            .then(application_credentials => {
                callback(application_credentials)
            })

    }

    fetchUser(
        uuid,

        callback
    ) {
        userModel.findOne({
            uuid
        })
            .lean()
            .exec()
            .then(user_credentials => {
                callback(user_credentials)
            })

    }

    fetchApplications(
        owner,

        callback
    ) {
        applicationModel.find({
            owner
        })
            .lean()
            .exec()
            .then(user_credentials => {
                callback(user_credentials)
            })

    }
}

module.exports = Storage