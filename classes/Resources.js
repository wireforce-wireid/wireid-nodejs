#!/usr/bin/env node

const yaml = require('yaml')
const fs = require('fs')
const path = require('path')
const oPath = require("object-path");

class Resources {
    #_strings_file = null
    #_strings_map = null

    /**
     * Resource management class
     */
    constructor() {
        this.#_strings_file = fs.readFileSync(path.normalize(__dirname + '/../resources/strings.yaml'), 'utf8')
        this.#_strings_map = yaml.parse(this.#_strings_file)
    }

    /**
     * Return file of strings
     * @returns {null|string}
     */
    get strings_file() {
        return this.#_strings_file;
    }

    /**
     * Return object of strings
     * @returns {null|array}
     */
    get strings_map() {
        return this.#_strings_map;
    }

    /**
     * Get string by path
     * @param path
     * @returns {*}
     */
    getString(path) {
        return oPath.get(this.strings_map, path, null)
    }
}

module.exports = Resources