const crypto = require("crypto-js");
const jwt = require('jsonwebtoken');

class Auth {
    generateKeychain(
        login,
        password,
    ) {
        return JSON.stringify({
            login,
            password,
        });
    }

    generateHmacPassword(
        keychain,
        password,
    ) {
        return crypto.HmacSHA1(password, keychain)
    }

    generateHmacKeychain(
        keychain,
        password,
    ) {
        const hmac_password = this.generateHmacPassword(keychain, password)

        return crypto.HmacSHA1(keychain, hmac_password)
    }

    generatePublicSecret(
        keychain,
        password
    ) {
        const hmac_password = this.generateHmacPassword(keychain, password)

        return "wpublic::" + crypto.SHA256(hmac_password)
    }

    generatePrivateSecret(
        keychain,
        password
    ) {
        const hmac_keychain = this.generateHmacKeychain(keychain, password)
        const public_secret = this.generatePublicSecret(keychain, password)

        return "wsecret::" + crypto.HmacMD5(hmac_keychain, public_secret).toString();
    }

    signSecretKeys(
        private_secret,
        public_secret
    ) {
        return crypto.HmacSHA1(private_secret, public_secret).toString()
    }

    tokenizeAccountAccess(
        public_secret,
        secret_sign
    ) {
        return jwt.sign({
            public_secret,
            secret_sign
        }, process.env.SECRET_JWT)
    }
}

module.exports = Auth