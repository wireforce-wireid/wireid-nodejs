class Grant {
    parse(grant) {
        const protocol = grant.toString().split('::')
        const namespace_data = protocol[1].toString().split('/')

        return {
            protocol: protocol[0],
            namespace: namespace_data[0],
            attr: namespace_data[1],
        }
    }
}

module.exports = Grant