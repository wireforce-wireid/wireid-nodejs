const auth = new (require("../classes/Auth"))();
const storage = new (require("../classes/Storage"))();
const middleware = new (require("../classes/Middleware"))();
const validate = new (require("../classes/Validate"))();
const resources = new (require("../classes/Resources"))();
const HTTPResponse = require('../classes/HTTPResponse')
const Controller = require('../classes/Controller')
const RouterSettingsScheme = require('../classes/RouterSettingsScheme')
const HTTPMethods = require("../classes/HTTPMethods");

const userModel = require('../models/user').userModel;
const credentialsModel = require('../models/credentials').credentialsModel;
const application = new (require("../classes/Application"))();

module.exports.auth = auth
module.exports.storage = storage
module.exports.middleware = middleware
module.exports.resources = resources
module.exports.HTTPResponse = HTTPResponse
module.exports.Controller = Controller
module.exports.RouterSettingsScheme = RouterSettingsScheme
module.exports.HTTPMethods = HTTPMethods
module.exports.validate = validate
module.exports.application = application

module.exports.userModel = userModel
module.exports.credentialsModel = credentialsModel
