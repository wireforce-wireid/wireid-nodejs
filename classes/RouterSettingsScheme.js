const HTTPMethods = require("./HTTPMethods");

class RouterSettingsScheme {
    #_path = "/"
    #_method = HTTPMethods.GET

    get path() {
        return this.#_path
    }

    set path(value) {
        this.#_path = value.toString();
    }

    get method() {
        return this.#_method;
    }

    set method(value) {
        this.#_method = value.toString();
    }

    constructor(path, method = HTTPMethods.GET) {
        this.path = path;
        this.method = method
    }
}

module.exports = RouterSettingsScheme