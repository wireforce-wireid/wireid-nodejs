#!/usr/bin/env node

class HTTPResponse {
    #_statusCode = 0
    #_message = null

    get statusCode() {
        return this.#_statusCode;
    }

    set statusCode(value) {
        this.#_statusCode = value;
    }

    get message() {
        return this.#_message;
    }

    set message(value) {
        this.#_message = value;
    }

    constructor(
        message = null,
        statusCode = 200,
    ) {
        this.statusCode = statusCode
        this.message = message
    }
}

module.exports = HTTPResponse