const Auth = require('./controllers/Auth')
const AuthLogin = require('./controllers/AuthLogin')
const AuthCreate = require('./controllers/AuthCreate')

const UserGet = require('./controllers/UserGet')
const UserCreate = require('./controllers/UserCreate')

const ApplicationCreate = require('./controllers/ApplicationCreate')
const ApplicationSign = require('./controllers/ApplicationSign')
const ApplicationGrant = require('./controllers/ApplicationGrant')
const ApplicationList = require('./controllers/ApplicationList')

module.exports.controllers = [
    Auth,
    AuthLogin,
    AuthCreate,

    UserGet,
    UserCreate,

    ApplicationCreate,
    ApplicationSign,
    ApplicationGrant,
    ApplicationList,
]