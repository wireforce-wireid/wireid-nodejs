#!/usr/bin/env node

const mongoose = require("mongoose");

const applicationSchema = new mongoose.Schema({
    uuid: String,
    name: String,
    app_id: String,
    grants: Array,
    owner: String,
    trusted: Array,
    rejected: Boolean,
    redirect_url: String,
}, {
    timestamps: true
});


const applicationModel = mongoose.model('application', applicationSchema);

module.exports.applicationSchema = applicationSchema
module.exports.applicationModel = applicationModel