#!/usr/bin/env node

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    uuid: String,
    first_name: String,
    last_name: String,
    language: String,
    birthday: Date,
    address: String,
}, {
    timestamps: true
});


const userModel = mongoose.model('user', userSchema);

module.exports.userSchema = userSchema
module.exports.userModel = userModel