const mongoose = require("mongoose");

const credentialsSchema = new mongoose.Schema({
    uuid: String,
    private_secret: String,
    public_secret: String,
}, {
    timestamps: true
});


const credentialsModel = mongoose.model('credential', credentialsSchema);

module.exports.credentialsSchema = credentialsSchema
module.exports.credentialsModel = credentialsModel