#!/usr/bin/env node

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPMethods,
    middleware,
    storage,
    auth
} = require("../classes/WireID");

class AuthLogin extends Controller {

    invoke(Request, Response) {
        const keychain = auth.generateKeychain(Request.body.login, Request.body.password)
        const public_secret = auth.generatePublicSecret(keychain, Request.body.password)
        const private_secret = auth.generatePrivateSecret(keychain, Request.body.password)
        const secret_sign = auth.signSecretKeys(private_secret, public_secret)

        storage.fetchUserCredentials(
            public_secret,
            secret_sign,

            function (error, userCredentials) {

                if (error === null) {
                    Response.json({
                        jwt: auth.tokenizeAccountAccess(
                            public_secret,
                            secret_sign
                        )
                    });
                } else {
                    Response.json({ success: false }, 500);
                }
            })
    }

    scheme() {
        return new RouterSettingsScheme(
            'auth/login',
            HTTPMethods.POST
        )
    }

    middleware(Request, Response, next) {
        return middleware.validateLoginForm(Request, Response, next)
    }
}

module.exports = AuthLogin