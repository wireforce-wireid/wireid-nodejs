#!/usr/bin/env node

const {v4: uuidv4} = require('uuid');

const credentialsModel = require('../models/credentials').credentialsModel;

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPMethods,
    middleware,
    storage,
    auth
} = require("../classes/WireID");

class AuthCreate extends Controller {

    invoke(Request, Response) {
        const keychain = auth.generateKeychain(Request.body.login, Request.body.password)
        const public_secret = auth.generatePublicSecret(keychain, Request.body.password)
        const private_secret = auth.generatePrivateSecret(keychain, Request.body.password)
        const secret_sign = auth.signSecretKeys(private_secret, public_secret)

        // Count accounts in storage
        storage.countUserCredentials(
            // User credentials
            Request.body.login,
            Request.body.password,

            // Callback
            async (count) => {

                if (count === 0) {
                    const credentials = new credentialsModel({
                        uuid: uuidv4(),
                        private_secret,
                        public_secret,
                    })

                    if (await credentials.save()) {
                        return Response.json({
                            jwt: auth.tokenizeAccountAccess(
                                public_secret,
                                secret_sign
                            )
                        }, 201);

                    } else {
                        return Response.json({message: this.R.getString('api.auth.not_created')}, 500);
                    }
                } else {
                    return Response.json({message: this.R.getString('api.auth.exist')}, 500);
                }
            }
        )
    }

    scheme() {
        return new RouterSettingsScheme(
            'auth/create',
            HTTPMethods.POST
        )
    }
    
    middleware(Request, Response, next) {
        return middleware.validateLoginForm(Request, Response, next)
    }

}

module.exports = AuthCreate