#!/usr/bin/env node

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPResponse,
    HTTPMethods,
    middleware,
    validate,
    storage,
    application
} = require("../classes/WireID");

class ApplicationSign extends Controller {

    invoke(Request, Response) {
        const sign = validate.getJSONDataFromJWT(Request.params.intent)
        const jwt_payload_authorization = validate.getJSONDataFromJWT(Request.headers.authorization)

        storage.fetchUserCredentials(
            jwt_payload_authorization.public_secret,
            jwt_payload_authorization.secret_sign,

            (error, userData) => {
                storage.fetchApplication(sign.app_id, (app) => {
                    if (!app) {
                        Response.json({ message: this.R.getString('api.application.not_found') }, 500)
                    } else {
                        try {
                            application.tokenizeAccess(Request.params.intent, userData.uuid,  (error, access) =>  {
                                if (error) {
                                    Response.json({ message: this.R.getString('api.application.not_auth') }, 500)
                                } else {
                                    Response.json(access, 200)
                                }
                            })

                        } catch (e) {
                            Response.json({ message: e.message }, 500)
                        }

                    }
                })
            }
        )


    }

    scheme() {
        return new RouterSettingsScheme(
            'application/sign/:intent',
            HTTPMethods.POST
        )
    }

    middleware(Request, Response, next) {
        return middleware.validateAuth(Request, Response, next)
    }
}

module.exports = ApplicationSign