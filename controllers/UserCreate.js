#!/usr/bin/env node

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPMethods,
    middleware,
    storage,
    userModel
} = require("../classes/WireID");

class UserCreate extends Controller {

    constructor(context) {
        super(context);

        this.native_middleware = [
            middleware.validateAuth,
            middleware.validateCreateUserForm,
        ]
    }

    invoke(Request, Response) {
        userModel.countDocuments({
            uuid: Request.userData.uuid,
        })
            .lean()
            .exec()
            .then(async associationRate => {
                if (associationRate === 0) {
                    const userData = new userModel({
                        uuid: Request.userData.uuid,
                        first_name: Request.body.first_name,
                        last_name: Request.body.last_name,
                        language: Request.body.language,
                        birthday: Request.body.birthday ? new Date(Request.body.birthday) : null,
                        address: Request.body.address,
                    });

                    const userCreated = await userData.save()

                    Response.json({
                        success: !!userCreated
                    }, userCreated ? 201 : 500)
                } else {
                    Response.json({
                        message: this.R.getString('api.user.already_assoc')
                    }, 500)

                }
            })
    }

    scheme() {
        return new RouterSettingsScheme(
            'user/create',
            HTTPMethods.POST
        )
    }
}

module.exports = UserCreate