#!/usr/bin/env node

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPResponse,
    HTTPMethods,
    middleware,
    storage,
    application
} = require("../classes/WireID");

class ApplicationList extends Controller {

    invoke(Request, Response) {
        storage.fetchApplications(Request.userData.uuid, function (apps) {
            Response.json({
                apps
            }, 200)
        })
    }

    scheme() {
        return new RouterSettingsScheme(
            'application/list',
            HTTPMethods.GET
        )
    }

    middleware(Request, Response, next) {
        return middleware.validateAuth(Request, Response, next)
    }
}

module.exports = ApplicationList