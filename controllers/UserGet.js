#!/usr/bin/env node

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPMethods,
    middleware,
    storage
} = require("../classes/WireID");

class UserGet extends Controller {

    invoke(Request, Response) {
        storage.fetchUser(Request.userData.uuid, function (user) {
            const { _id, __v, ...updatedUser } = user;

            Response.json({
                user: updatedUser
            }, 200)
        })
    }

    scheme() {
        return new RouterSettingsScheme(
            'user/get',
            HTTPMethods.GET
        )
    }

    middleware(Request, Response, next) {
        return middleware.validateAuth(Request, Response, next)
    }
}

module.exports = UserGet