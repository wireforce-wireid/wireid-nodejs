#!/usr/bin/env node

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPResponse,
    HTTPMethods,
    middleware
} = require("../classes/WireID");

class Auth extends Controller {

    invoke(Request, Response) {
       return new HTTPResponse({ success: true }, 200)
    }

    scheme() {
        return new RouterSettingsScheme(
            'auth/',
            HTTPMethods.POST
        )
    }

    middleware(Request, Response, next) {
        return middleware.validateAuth(Request, Response, next)
    }
}

module.exports = Auth