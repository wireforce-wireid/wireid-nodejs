#!/usr/bin/env node
const {v4: uuidv4} = require('uuid');

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPResponse,
    HTTPMethods,
    middleware,
    storage,
    application
} = require("../classes/WireID");

class ApplicationCreate extends Controller {

    constructor(context) {
        super(context);
        
        this.native_middleware = [
            middleware.validateCreateApplicationForm,
            middleware.validateAuth
        ]
    }


    invoke(Request, Response) {
        const uuid = uuidv4()
        const app_id = application.signApplicationId(uuid, Request.userData.private_secret)

        storage.countApplicationCredentials(
            Request.body.app_name,
            Request.body.redirect_url,

            async (count) => {
                if (count === 0) {

                    // %TRUSTED_APP:CONSOLE%/auth
                    const applicationData = await application.create(
                        uuid,
                        Request.body.app_name,
                        app_id,
                        Request.body.grants,
                        Request.userData.uuid,
                        Request.body.redirect_url,
                    )

                    if (applicationData) {
                        Response.json({
                            success: true,
                            app_id
                        })
                    } else {
                        Response.json({
                            success: false
                        })
                    }

                } else {
                    Response.json({ message: this.R.getString('api.application.exist') }, 500)
                }
            }
        )
    }

    scheme() {
        return new RouterSettingsScheme(
            'application/create',
            HTTPMethods.POST
        )
    }
}

module.exports = ApplicationCreate