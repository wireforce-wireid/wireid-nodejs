#!/usr/bin/env node

// WireID Framework
const {
    Controller,
    RouterSettingsScheme,
    HTTPResponse,
    HTTPMethods,
    middleware,
    validate,
    application,
    storage
} = require("../classes/WireID");

class ApplicationGrant extends Controller {

    invoke(Request, Response) {
        
        storage.fetchApplication(Request.params.app_id, (app) => {
            if (!app) {
                Response.json({ message: this.R.getString('api.application.not_found') }, 500)
            } else {
                Response.json({
                    name: app.name,
                    uuid: app.uuid,
                    app_id: app.app_id,
                    grants: app.grants,

                    intent: application.signTransactionAccess(
                        app.app_id,
                        Request.userData.public_secret
                    )
                }, 200)
            }
        })

    }

    scheme() {
        return new RouterSettingsScheme(
            'application/grant/:app_id',
            HTTPMethods.POST
        )
    }

    middleware(Request, Response, next) {
        return middleware.validateAuth(Request, Response, next)
    }
}

module.exports = ApplicationGrant