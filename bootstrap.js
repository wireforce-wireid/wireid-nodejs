// const onetime = require("onetime");

// WireID Framework
const auth = new (require("./classes/Auth"))();
const storage = new (require("./classes/Storage"))();
const middleware = new (require("./classes/Middleware"))();
const resources = new (require("./classes/Resources"))();

const path = require("path");
const express = require("express");
const map = require('lodash').map

const Context = require("./classes/Context");
const HTTPResponse = require('./classes/HTTPResponse')

const controllers = require('./schemas').controllers

const {parsePath} = require("./classes/Controller");

/**
 * Bootstrap application
 * @param expressApplication
 * @returns {{routes: *, mutatedExpressApplication: *}}
 */
function bootstrap(
    expressApplication,
) {

    /**
     * Create base context
     * @type {Context}
     */
    const applicationContext = new Context(
        resources,
        expressApplication
    )

    const routes = map(
        map(controllers, controller => {
            return new controller(applicationContext)
        }
    ),
        /**
         * Register each controller
         * @param controller {Controller}
         */
        (controller) => {

            // Setup gates
            const gates = (typeof controller.middleware === 'object') ? [controller.middleware] : controller.middleware

            /**
             * Bootstrap controller
             * @type {RouterSettingsScheme}
             */
            const controllerScheme = controller.scheme()

            if (controllerScheme.path) {
                const parsedPath = parsePath(controllerScheme.path)

                const normalizeParentPath = ('/' + parsedPath.parentPath).replace('//', '/')
                const normalizePath = ('/' + parsedPath.path).replace('//', '/')

                // Set path by scheme function
                controller.parentPath = normalizeParentPath ?? controller.parentPath
                controller.path = normalizePath ?? controller.path
                controller.method = controllerScheme.method ?? controller.method
            }

            /**
             * Create local router
             * @type {*|Router}
             */
            const router = express.Router();

            router.all(controller.path,

                controller.native_middleware ? controller.native_middleware : gates,

                /**
                 *
                 * @param req {Request}
                 * @param res {Response}
                 * @param next {*}
                 */
                function (req, res, next) {
                    if (req.method.toString().toLowerCase() === controller.method.toLowerCase()) {
                        const invoked = controller.invoke(req, res)

                        if (invoked instanceof HTTPResponse) {
                            res.send(invoked.message, invoked.statusCode)
                        }
                    } else {
                        next();
                    }
                }
            );

            expressApplication.use(controller.parentPath, router)

            // Report about controller register
            return {
                controller_name: controller.constructor.name,
                method: controller.method.toUpperCase(),
                parent_path: controller.parentPath,
                path: controller.path,
            }
        }
    )

    return {
        routes,
        mutatedExpressApplication: expressApplication
    }
}

module.exports = bootstrap

// const routerSetup = onetime(() => {
//
// })